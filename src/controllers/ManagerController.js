
const express = require('express');
const path = require('path');
const dataManager = require(path.join(__dirname, '../tool/mysql.js'));
const xtpl = require('xtpl');
const data_config=require(path.join(__dirname,'../tool/data_config.js'))
const index_lunbo = path.join(__dirname, '../views/index_lunbo.html');





//获取首页轮播数据
exports.get_index_lunbo=(req,res)=>{

	dataManager.find('lunbo','index_status', (err,docs)=>{
		if (err) throw err;
		docs.forEach((v,i)=>{
			v.dataName = 'index_lunbo';
			v.index_img_src=v.index_img_src && v.index_img_src.replace('../../','')
		})
		xtpl.renderFile(index_lunbo, {
			list: data_config.data_config(data_config.deal_data(['id','index_img_src', 'index_img_target','dataName'], docs)),
			loginedname: req.session.username
		},  (error, content)=> {
			//设置响应头
			res.setHeader("Content-Type", "text/html;charset='utf-8'");

			//返回数据
			res.end(content);
		});
	})
}

//获取活动轮播
const activity_lunbo = path.join(__dirname, '../views/activity_lunbo.html');

exports.get_activity_lunbo=(req,res)=>{
	
	dataManager.find('lunbo','activity_status',(err,docs)=>{
		if (err) {
			throw err;
		}
		docs.forEach((v,i)=>{
			v.dataName = 'activity_lunbo';
			v.index_img_src=v.index_img_src.replace('../../','')
		})
		xtpl.renderFile(activity_lunbo,{
			list: data_config.data_config(data_config.deal_data(['id','activity_img_src', 'activity_img_target','dataName'], docs)),
			loginedname: req.session.username
		},(error, content)=> {
			//设置响应头
			res.setHeader("Content-Type", "text/html;charset='utf-8'");

			//返回数据
			res.end(content);
		})
	})
}

//获取活动数据
const activity = path.join(__dirname, '../views/activity.html');

exports.get_activity=(req,res)=> {

		dataManager.find("activity",'status', (err, docs, fields)=> {
			if (err) throw err;
			docs.forEach((v,i)=>{
				v.dataName = 'activity';
				v.small_img_src = v.small_img_src.replace('../../', '');
				v.big_img_src = v.big_img_src.replace('../../', '');
			})
			xtpl.renderFile(activity, {
				list: data_config.data_config(data_config.deal_data('',docs)),
				loginedname: req.session.username
			}, function (error, content) {
				//设置响应头
				res.setHeader("Content-Type", "text/html;charset='utf-8'");

				//返回数据
				res.end(content);
			});
		})
}
//获取专家数据
const expert = path.join(__dirname, '../views/expert.html');

exports.get_expert=(req,res)=>{

	dataManager.findId("experts_news2",'expert',(err,docs)=>{
		let docs_arr = [];
		if (docs[0].status==0) {
			docs[0].dataName = 'expert';
			docs[0].query = 5;
			docs_arr.push(docs[0])
		}
		xtpl.renderFile(expert,{
			list:docs_arr,
			loginedname:req.session.username
		},function(error,content){
			//设置响应头
			res.setHeader("Content-Type", "text/html;charset='utf-8'");

			//返回数据
			res.end(content);
		});
	})
}

//获取热点消息数据
const news = path.join(__dirname, '../views/news.html');
exports.get_news=(req,res)=>{

	dataManager.findId("experts_news2",'news',(err,docs)=>{
		let docs_arr = [];
		if (docs[0].status==0) {
			docs[0].dataName = 'news';
			docs[0].query = 4;
			docs_arr.push(docs[0])
		}
		xtpl.renderFile(news,{
			list:docs_arr,
			loginedname:req.session.username
		},function(error,content){
			//设置响应头
			res.setHeader("Content-Type", "text/html;charset='utf-8'");

			//返回数据
			res.end(content);
		});
	})
}

//获取首页专家数据
const index_expert = path.join(__dirname, '../views/index_expert.html');
exports.get_index_expert=(req,res)=>{

	dataManager.findId("experts_news2",'index_expert',(err,docs)=>{
		let docs_arr = [];
		if (docs[0].status==0) {
			docs[0].dataName = 'index_expert';
			docs[0].query = 19;
			docs_arr.push(docs[0])
		}
		xtpl.renderFile(index_expert,{
			list:docs_arr,
			loginedname:req.session.username
		},function(error,content){
			//设置响应头
			res.setHeader("Content-Type", "text/html;charset='utf-8'");

			//返回数据
			res.end(content);
		});
	})
}



//获取新增页面
exports.getAddPage=(req,res)=>{
	let queryArr = req.url.split('?')[1].split('&').splice(1)
	let data=data_config.toData(queryArr)
	let dataName = req.url.split('?')[1].split('&')[0];
	data[0].push (dataName);

	xtpl.renderFile(path.join(__dirname,'../views/add.html'),{
		list:data,
		loginedname:req.session.username
	},(err,data)=>{
		//设置响应头
		res.setHeader("Content-Type","text/html;charset=utf-8");

		//返回数据
		res.end(data);
	})
}

//专家图片地址
const expert_img = path.join(__dirname, '../views/expert_img.html');
exports.get_expert_img=(req,res)=>{
	dataManager.findId("experts_news2",'expert_img',(err,docs)=>{
		let docs_arr = [];
		if (docs[0].status==0) {
			docs[0].dataName = 'expert_img';
			docs[0].query = 20;
			docs_arr.push(docs[0])
		}
		xtpl.renderFile(expert_img,{
			list:docs_arr,
			loginedname:req.session.username
		},function(error,content){
			//设置响应头
			res.setHeader("Content-Type", "text/html;charset='utf-8'");

			//返回数据
			res.end(content);
		});
	})
}








//增加首页轮播信息
exports.add_index_lunbo=(req,res)=>{

	const multiparty = require('multiparty');
	const form = new multiparty.Form();//新建表单
//设置编辑
	form.encoding = 'utf-8';
//设置图片存储路径
	form.uploadDir = "images/";
	form.keepExtensions = true;   //保留后缀
	form.maxFieldsSize = 2*1024*1024; //内存大小
	form.maxFilesSize= 5*1024*1024;//文件字节大小限制，超出会报错err

	form.parse(req, function(err,fields,files) {
		//获取路径
		var oldpath=files.index_img_src[0]['path'];

		//文件后缀处理格式
		if(oldpath.indexOf('.jpg')>=0){
			var suffix='.jpg';
		}else if(oldpath.indexOf('.png')>=0){
			var suffix='.png';
		}else if(oldpath.indexOf('.gif')>=0){
			var suffix='.gif';
		}else{
			var u={"error" :1,"message":'请上传正确格式'};
			res.end(JSON.stringify(u));
			return false;
		}

		var img_url='images/'+Date.now()+suffix;
		var fs=require('fs');
		//给图片修改名称
		fs.renameSync(oldpath,img_url);



		let addInfo = {
			id: fields.id[0],
			index_img_src: '../../../../'+img_url,
			index_img_target: fields.index_img_target[0],
			index_status:0
		};

		dataManager.findOne('lunbo',addInfo.id,(err,doc)=>{
			//查询有结果就修改  无结果则添加
			if (doc[0]!=null) {
				dataManager.edit_index_lunbo([addInfo.index_img_src,addInfo.index_img_target,0,addInfo.id], (err, result)=> {

				});
			}else {
				dataManager.add('lunbo',addInfo,(err,result)=>{

				})
			}
			res.end('<script>window.location="/manager/index_lunbo"</script>')
		})


	});


}

//增加活动轮播信息
exports.add_activity_lunbo=(req,res)=>{
	const multiparty = require('multiparty');
	const form = new multiparty.Form();//新建表单
//设置编辑
	form.encoding = 'utf-8';
//设置图片存储路径
	form.uploadDir = "images/";
	form.keepExtensions = true;   //保留后缀
	form.maxFieldsSize = 2*1024*1024; //内存大小
	form.maxFilesSize= 5*1024*1024;//文件字节大小限制，超出会报错err

	form.parse(req, function(err,fields,files) {
		//获取路径

		var oldpath=files.activity_img_src[0]['path'];

		//文件后缀处理格式
		if(oldpath.indexOf('.jpg')>=0){
			var suffix='.jpg';
		}else if(oldpath.indexOf('.png')>=0){
			var suffix='.png';
		}else if(oldpath.indexOf('.gif')>=0){
			var suffix='.gif';
		}else{
			var u={"error" :1,"message":'请上传正确格式'};
			res.end(JSON.stringify(u));
			return false;
		}

		var img_url='images/'+Date.now()+suffix;
		var fs=require('fs');
		//给图片修改名称
		fs.renameSync(oldpath,img_url);




		let addInfo = {
			id: fields.id[0],
			activity_img_src: '../../../../'+img_url,
			activity_img_target: fields.activity_img_target[0],
			activity_status:0
		};



		dataManager.findOne('lunbo',addInfo.id,(err,doc)=>{
			//查询有结果就修改  无结果则添加
			if (doc[0]!=null) {
				dataManager.edit_activity_lunbo([addInfo.activity_img_src,addInfo.activity_img_target,0,addInfo.id],(err,result)=>{

				})
			}else {
				dataManager.add('lunbo',addInfo,(err,result)=>{

				})
			}
			res.end('<script>window.location="/manager/activity_lunbo"</script>')
		})





	});


}




//增加活动信息
exports.add_activity=(req,res)=>{

	const multiparty = require('multiparty');
	const form = new multiparty.Form();//新建表单
//设置编辑
	form.encoding = 'utf-8';
//设置图片存储路径
	form.uploadDir = "images/";
	form.keepExtensions = true;   //保留后缀
	form.maxFieldsSize = 2*1024*1024; //内存大小
	form.maxFilesSize= 5*1024*1024;//文件字节大小限制，超出会报错err
	form.parse(req, function(err,fields,files) {
		//获取路径
		var oldpath1=files.small_img_src[0]['path'];
		var oldpath2=files.big_img_src[0]['path'];

		var img_url1='images/'+Date.now()+'.'+oldpath1.split('.')[1];
		var img_url2='images/'+Date.now()+'.'+oldpath2.split('.')[1];

		var fs=require('fs');
		//给图片修改名称
		fs.renameSync(oldpath1,img_url1);
		fs.renameSync(oldpath2,img_url2);



		let addInfo = {
			id: fields.id[0],
			teachers: fields.teachers[0],
			school: fields.school[0],
			speechTitle: fields.speechTitle[0],
			place: fields.place[0],
			people: fields.people[0],
			sponsor: fields.sponsor[0],
			content: fields.content[0],
			small_img_src: '../../../../'+img_url1,
			big_img_src: '../../../../'+img_url2,
			start_time: fields.start_time[0],
			end_time: fields.end_time[0],
			longitude: fields.longitude[0],
			latitude: fields.latitude[0],
			status:0
		};

		dataManager.findOne('activity',addInfo.id,(err,doc)=>{
			//查询有结果就修改  无结果则添加
			if (doc[0]!=null) {
				dataManager.edit_activity([addInfo.teachers,addInfo.school,addInfo.speechTitle,addInfo.place,addInfo.people,addInfo.sponsor,addInfo.content,addInfo.small_img_src,addInfo.big_img_src,addInfo.start_time,addInfo.end_time,addInfo.longitude,addInfo.latitude,addInfo.id],(err,result)=>{

				})
			}else {
				dataManager.add('activity',addInfo,(err,result)=>{

				})
			}
			res.end('<script>window.location="/manager/activity"</script>')
		})

	});







}

//增加专家信息
exports.add_expert=(req,res)=>{
	let addInfo = {
		id: 'expert',
		html: req.body.expert_html,
		status:0
	};
	dataManager.findId('experts_news2',addInfo.id,(err,doc)=>{
		//查询有结果就修改  无结果则添加
		if (doc[0]!=null) {
			dataManager.edit_expert([addInfo.html,0,addInfo.id],(err,result)=>{

			})
		}else {
			dataManager.add('experts_news2',addInfo,(err,result)=>{

			})
		}
		res.end('<script>window.location="/manager/expert"</script>')
	})
	// dataManager.add('info', addInfo, (docs)=> {
	// 	res.end('<script>window.location="/manager/list"</script>')
	// });
}

//增加热点消息信息
exports.add_news=(req,res)=>{
	let addInfo = {
		id: 'news',
		html: req.body.news_html,
		status:0
	};
	dataManager.findId('experts_news2',addInfo.id,(err,doc)=>{
		//查询有结果就修改  无结果则添加
		if (doc[0]!=null) {
			dataManager.edit_news([addInfo.html,0,addInfo.id],(err,result)=>{

			})
		}else {
			dataManager.add('experts_news2',addInfo,(err,result)=>{

			})
		}
		res.end('<script>window.location="/manager/news"</script>')
	})
	// dataManager.add('info', addInfo, (docs)=> {
	// 	res.end('<script>window.location="/manager/list"</script>')
	// });
}

//增加首页专家信息
exports.add_index_expert=(req,res)=>{
	let addInfo = {
		id: 'index_expert',
		html: req.body.index_expert_html,
		status:0
	};
	dataManager.findId('experts_news2',addInfo.id,(err,doc)=>{
		//查询有结果就修改  无结果则添加
		if (doc[0]!=null) {
			dataManager.edit_index_expert([addInfo.html,0,addInfo.id],(err,result)=>{

			})
		}else {
			dataManager.add('experts_news2',addInfo,(err,result)=>{

			})
		}
		res.end('<script>window.location="/manager/index_expert"</script>')
	})
}

//增加专家图片
exports.add_expert_img=(req,res)=>{
	const multiparty = require('multiparty');
	const form = new multiparty.Form();//新建表单
//设置编辑c
	form.encoding = 'utf-8';
//设置图片存储路径
	form.uploadDir = "images/";
	form.keepExtensions = true;   //保留后缀
	form.maxFieldsSize = 2*1024*1024; //内存大小
	form.maxFilesSize= 5*1024*1024;//文件字节大小限制，超出会报错err
	form.parse(req, function(err,fields,files) {
		//获取路径
	
		var oldpath=files.expert_img[0]['path'];
		//文件后缀处理格式
		if(oldpath.indexOf('.jpg')>=0){
			var suffix='.jpg';
		}else if(oldpath.indexOf('.png')>=0){
			var suffix='.png';
		}else if(oldpath.indexOf('.gif')>=0){
			var suffix='.gif';
		}else{
			var u={"error" :1,"message":'请上传正确格式'};
			res.end(JSON.stringify(u));
			return false;
		}

		var img_url='images/'+Date.now()+suffix;
		var fs=require('fs');
		//给图片修改名称
		fs.renameSync(oldpath,img_url);


		let addInfo = {
			id: 'expert_img',
			html: img_url.split('/')[1],
			activity_status:0
		};



		dataManager.findId('experts_news2',addInfo.id,(err,doc)=>{
			//查询有结果就修改  无结果则添加
			if (doc[0]!=null) {
				dataManager.edit_index_expert([addInfo.html,0,addInfo.id],(err,result)=>{

				})
			}else {
				dataManager.add('experts_news2',addInfo,(err,result)=>{

				})
			}
			res.end('<script>window.location="/manager/expert_img"</script>')
		})

	});

}




//获取首页轮播修改页面
exports.getEditPage_index_lunbo=(req,res)=>{
	dataManager.findOne('lunbo',req.query.id,(err,doc)=>{
		doc[0].dataName = req.query.dataName;
		xtpl.renderFile(path.join(__dirname,'../views/index_lunbo_edit.html'), {
			Info:doc[0],
			loginedname:req.session.username
			},(err,content)=>{
			//设置响应头
			if (err) {
				console.log(err);
			}
			res.setHeader("Content-Type","text/html;charset=utf-8");

			//返回数据
			res.end(content);
		});
	});
}

//获取活动页轮播修改页面
exports.getEditPage_expert_lunbo=(req,res)=>{
	dataManager.findOne('lunbo',req.query.id,(err,doc)=>{
		doc[0].dataName = req.query.dataName;
		xtpl.renderFile(path.join(__dirname,'../views/activity_lunbo_edit.html'),
			{
				Info:doc[0],
				loginedname:req.session.username
			}
			,(err,content)=>{
			//设置响应头
			if (err) {
				console.log(err);
			}
			res.setHeader("Content-Type","text/html;charset=utf-8");

			//返回数据
			res.end(content);
		});
	});
}

//获取活动修改页面
exports.getEditPage_activity=(req,res)=>{
	dataManager.findOne('activity',req.query.id,(err,doc)=>{
		doc[0].dataName = req.query.dataName;
		xtpl.renderFile(path.join(__dirname,'../views/activity_edit.html'),
			{
				Info:doc[0],
				loginedname:req.session.username
			},(err,content)=>{
			//设置响应头
			if (err) {
				console.log(err);
			}
			res.setHeader("Content-Type","text/html;charset=utf-8");

			//返回数据
			res.end(content);
		});
	});
}

//获取专家修改页面
exports.getEditPage_expert=(req,res)=>{
	dataManager.findId('experts_news2',req.query.id,(err,doc)=>{
		doc[0].dataName = req.query.dataName;
		xtpl.renderFile(path.join(__dirname,'../views/expert_edit.html'),
			{
				Info:doc[0],
				loginedname:req.session.username
			},(err,content)=>{
			//设置响应头
			if (err) {
				console.log(err);
			}
			res.setHeader("Content-Type","text/html;charset=utf-8");

			//返回数据
			res.end(content);
		});

	});
}

//获取热点消息修改页面
exports.getEditPage_news=(req,res)=>{
	dataManager.findId('experts_news2',req.query.id,(err,doc)=>{
		doc[0].dataName = req.query.dataName;
		xtpl.renderFile(path.join(__dirname,'../views/news_edit.html'),
			{
				Info:doc[0],
				loginedname:req.session.username
			},(err,content)=>{
			//设置响应头
			if (err) {
				console.log(err);
			}
			res.setHeader("Content-Type","text/html;charset=utf-8");

			//返回数据
			res.end(content);
		});
	});
}

//获取首页专家修改页面
exports.getEditPage_index_expert=(req,res)=>{
	dataManager.findId('experts_news2',req.query.id,(err,doc)=>{
		doc[0].dataName = req.query.dataName;
		xtpl.renderFile(path.join(__dirname,'../views/index_expert_edit.html'),
			{
				Info:doc[0],
				loginedname:req.session.username
			},(err,content)=>{
				//设置响应头
				if (err) {
					console.log(err);
				}
				res.setHeader("Content-Type","text/html;charset=utf-8");

				//返回数据
				res.end(content);
			});

	});
}








//删除信息
exports.delete_data=(req,res)=>{
	const resultObj = {status:1,message:"删除成功"};
	dataManager.delete_data(req.query.dataName, req.query.status, [1, req.query.id], (result)=> {
		//设置响应头
		res.setHeader("Content-Type", "application/json;charset=utf-8");

		res.json(resultObj);

	});
}


