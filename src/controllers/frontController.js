const express = require('express');
const path = require('path');
const dataManager = require(path.join(__dirname, '../tool/mysql.js'));
const data_config=require(path.join(__dirname,'../tool/data_config.js'))
const fs = require('fs');
//获取首页轮播图数据
exports.index_lunbo=(req,res)=>{
	res.header("Access-Control-Allow-Origin", "*");
	dataManager.find('lunbo','index_status',(err,docs)=>{
		if (err) throw err;

		//返回数据
		res.end(JSON.stringify(docs))
	})
}

//获取活动页轮播图数据
exports.activity_lunbo=(req,res)=>{
	res.header("Access-Control-Allow-Origin", "*");
	dataManager.find('lunbo','activity_status',(err,docs)=>{
		if (err) throw err;
		//返回数据
		res.end(JSON.stringify(docs))
	})
}

//获取活动页数据
exports.activity=(req,res)=>{
	res.header("Access-Control-Allow-Origin", "*");
	dataManager.find('activity','status',(err,docs)=>{
		if (err) throw err;

		//返回数据
		res.end(JSON.stringify(docs))
	})
}

//获取某个详细活动数据
exports.activity_detail=(req,res)=>{
	res.header("Access-Control-Allow-Origin", "*");
	dataManager.findOne('activity',req.params.activity_id,(err,docs)=>{
		if (err) throw err;
		//返回数据
		res.end(JSON.stringify(docs))
	})
}

//获取专家数据
exports.expert=(req,res)=>{
	res.header("Access-Control-Allow-Origin", "*");
	dataManager.find('experts_news2','status',(err,docs)=>{
		if (err) throw err;

		//返回数据
		res.end(JSON.stringify(docs))
	})
}

//获取首页热点消息数据
exports.news=(req,res)=>{
	res.header("Access-Control-Allow-Origin", "*");
	dataManager.find('experts_news2','status',(err,docs)=>{
		if (err) throw err;

		//返回数据
		res.end(JSON.stringify(docs))
	})
}

//获取首页专家数据
exports.index_expert=(req,res)=>{
	res.header("Access-Control-Allow-Origin", "*");
	dataManager.find('experts_news2','status',(err,docs)=>{
		if (err) throw err;

		//返回数据
		res.end(JSON.stringify(docs))
	})
}




//处理首页
exports.index=(req,res)=>{
	fs.readFile(path.join(__dirname,'../../dist/index.html'),(err,data)=>{
		if (err) {
			throw err;
		}

		//设置响应头
		res.setHeader("Content-Type","text/html;charset=utf-8");
		//返回数据
		res.end(data);

	})
}