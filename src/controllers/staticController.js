const express = require('express');
const path = require('path');
const fs = require('fs');

//处理首页
exports.index=(req,res)=>{
	fs.readFile(path.join(__dirname,'../../dist/index.html'),(err,data)=>{
		if (err) {
			throw err;
		}

		//设置响应头
		res.setHeader("Content-Type","text/html;charset=utf-8");
		//返回数据
		res.end(data);

	})
}


//处理app.js
exports.appJs=(req,res)=>{
	fs.readFile(path.join(__dirname,'../../dist/static/js/app.js'),(err,data)=>{
		if (err) {
			throw err;
		}

		//设置响应头
		res.setHeader("Content-Type","application/x-javascript");
		//返回数据
		res.end(data);
	})
}

//处理app.css
exports.appCss=(req,res)=>{
	fs.readFile(path.join(__dirname,'../../dist/static/css/app.css'),(err,data)=>{
		if (err) {
			throw err;
		}

		//设置响应头
		res.setHeader("Content-Type","text/css");
		//返回数据
		res.end(data);
	})
}


//处理vendor.js
exports.vendorJs=(req,res)=>{
	console.log(111);
	fs.readFile(path.join(__dirname,'../../dist/static/js/vendor.js'),(err,data)=>{
		if (err) {
			throw err;
		}

		//设置响应头
		res.setHeader("Content-Type","application/x-javascript");
		//返回数据
		res.end(data);
	})
}


//处理manifest.js
exports.manifestJs=(req,res)=>{
	fs.readFile(path.join(__dirname,'../../dist/static/js/manifest.js'),(err,data)=>{
		if (err) {
			throw err;
		}

		//设置响应头
		res.setHeader("Content-Type","application/x-javascript");
		//返回数据
		res.end(data);
	})
}

//图片处理
exports.img=(req,res)=>{

	fs.readFile(path.join(__dirname, '../../images/'+req.params.img_id), (err, data) => {
		if (err) throw err;
		res.writeHead(200, {'Content-Type': 'image/jpeg'});
		res.end(data);
	});


}