/**
 *所有账号的逻辑请求就写在这里
 * 
 */
//导包
const fs = require('fs')
const path = require('path')
const dataManager = require(path.join(__dirname, '../tool/datamanager.js'));

//获取登录页面
exports.getLoginPage = (req,res)=>{
    //处理获取登录页面的逻辑
    fs.readFile(path.join(__dirname,'../views/login.html'),(err,data)=>{
        if (err) {
          console.log(err);
        }

        //设置响应头
        res.setHeader("Content-Type","text/html;charset=utf-8");

        //返回数据
        res.end(data);
    })
}

//处理登录逻辑

exports.login = (req, res)=> {
	if (req.body.uname==='admin' && req.body.pwd=='123') {
		req.session.username = req.body.uname;

		res.end("<script>window.location.href='/manager/index_lunbo'</script>")
	}else {
		//设置响应头
		res.setHeader("Content-Type","text/html;charset=utf-8");
		res.end("<script>alert('账号或者密码输入错误，请重新登录');window.location.href='/account/login'</script>")
	}



	// dataManager.findOne("account",{username: req.body.uname, password: req.body.pwd, status: 0},(docs)=>{
	// 	if (docs!=null) {
	// 		//增加服务器端缓存
	// 		req.session.username = req.body.uname;
	// 		res.end("<script>window.location.href='/manager/index_lunbo'</script>")
	// 	}else {
	// 		res.end("<script>alert('账号或者密码输入错误，请重新登录');window.location.href='/account/login'</script>")
	// 	}
	// })

};

//处理退出逻辑
exports.logout=(req,res)=>{
	//1.把session中username置为null
	req.session.username = null;

	//2.跳转到登录页面
	res.end("<script>window.location.href='/account/login';</script>");
}



//获取注册页面
exports.getRegisterPage = (req,res)=>{
	fs.readFile(path.join(__dirname,'../views/register.html'),(error,data)=>{
		//返回给浏览器注册页面
		res.end(data);
	})
}

//处理注册逻辑
exports.register=(req,res)=>{
	
	dataManager.add('account',{username:req.body.uname,password:req.body.pwd,status:0},(result)=>{
		const resultObj = {status: 1, message: "注册成功"};
		if (result==null) {
			resultObj.status = 0;
			resultObj.message = "注册失败"
			res.json(resultObj)
		}else {
			res.json(resultObj)
		}

	})
}