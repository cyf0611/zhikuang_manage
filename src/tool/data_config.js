const express = require('express');
const arr=[
	{index_img_src:'首页轮播图片'},
	{index_img_target:'首页轮播图片跳转地址'},
	{activity_img_src:'活动轮播图片'},
	{activity_img_target:'活动轮播图片跳转地址'},
	{news_html:'热点消息代码'},
	{expert_html:'专家数据代码'},
	{teachers:'演讲人'},
	{school:'所在学习/公司'},
	{speechTitle:'演讲题目'},
	{place:'演讲地点'},
	{people:'参加人数'},
	{sponsor:'举办单位'},
	{content:'演讲内容'},
	{small_img_src:'小图片'},
	{big_img_src:'大图片'},
	{start_time:'活动开始时间'},
	{end_time:'活动结束时间'},
	{longitude:'经度'},
	{latitude:'纬度'},
	{index_expert_html:'首页导师数据代码'},
	{expert_img:'上传导师图片'}
]

//将数据加入query属性，索引对应相应的名字。确保点击新增按钮时  新增页面动态变化
exports.data_config=(obj1)=>{
	var docs = [];
	obj1.forEach((v,index)=>{
		var obj=obj1[index]
		var res = [];
		for (var key in obj) {
			arr.forEach((v,i)=>{
				for (let k in v) {
					if (key===k) {
						res.push(i)
					}
				}
			})
		}
		obj.query = res.join('&');
		docs.push(obj)
	})

	return docs;
}
//跳转到新增页面 将query对应的数字映射成相应的名字
exports.toData=(Array)=>{
	let res = [];
	Array.forEach((v)=>{
		let arr1 = [];
		for (let key in arr[Number(v)]) {
			arr1.push(key)
			arr1.push(arr[Number(v)][key])
		}
		res.push(arr1)
	})
	return res;
}

//功能：一个对象多个属性，指定相应的属性名字，返回只含有该属性的对象
exports.deal_data=(Array,data_arr)=>{
	let res = [];
	let a = 0;
	if (typeof Array=='object') {
		let middle_arr=[]
		Array.forEach((v,i)=>{
			for (let index in data_arr) {
				let obj=data_arr[index]
				if (obj.status!=1) {
					a++;
					for (let key in obj) {
						if (v===key) {
							middle_arr.push(obj[v])
						}
					}
				}
			}
		})


		for(var i=0;i<middle_arr.length/Array.length;i++){
			var res_obj = {};
			Array.forEach((v,j)=>{
				res_obj[v] = middle_arr[i+a/Array.length*j];
			})
			res.push(res_obj)
		}


	}else {
		for(let k in data_arr) {
			if (data_arr[k].status!=1) {
				res.push(data_arr[k]);
			}
		}
	}
	return res;
}
