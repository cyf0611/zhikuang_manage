
const MongoClient = require('mongodb').MongoClient

var url = 'mongodb://localhost:27017/bghmqd05';

exports.ObjectId = require('mongodb').ObjectId

MongoClient.connect(url, function(err, db) {

	exports.findOne=(dataname,collect,callback)=>{
	var collection = db.collection(dataname);
	collection.findOne(collect,function (err, docs) {
		if (err) {
			console.log(err);
		}
		callback(docs);

	});
	}

	//获取学生列表信息
	exports.findMany=(dataname,collect,callback)=>{
		var collection = db.collection(dataname);

		collection.find(collect).toArray(function(err, docs) {
			if (err) {
				console.log(err);
			}
			callback(docs);

		});
	}

	//增加学生信息
	exports.add=(dataname,collect,callback)=>{
		var collection = db.collection(dataname);
		// Insert some documents
		collection.insertOne(collect, function(err, result) {
			if (err) {
				console.log(err);
			}
			callback(result);
		});
	}

	//修改信息
	exports.edit=(dataname,collect,content,callback)=>{
		var collection = db.collection(dataname);
		collection.updateOne(collect, { $set: content }, function(err, result) {
				if (err) {
					console.log(err);
				}
				callback(result);
			});
	}

	//删除信息
	exports.delete=(dataname,collect,callback)=>{
		var collection = db.collection(dataname);
		// Insert some documents
		collection.deleteOne(collect, function(err, result) {
			if (err) {
				console.log(err);
			}
			callback(result);
		});
	}
});

