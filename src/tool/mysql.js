var mysql      = require('mysql');
var connection = mysql.createConnection({
	host     : '10.11.3.230',
	user     : 'linq',
	password : 'linq',
	database : 'finance_pro'
});
connection.connect();


//查询无限定条件数据
exports.find= (dataName,status,callback)=> {
	if (typeof status !=='string') {
		status = 'status';
	}
	//connection.query('SELECT * FROM '+dataName, callback);
	connection.query('SELECT * FROM '+dataName+' where '+status+'="0"', callback);
}
//查询限定id数据
exports.findOne= (dataName,id,callback)=> {
	connection.query('SELECT * FROM '+dataName+' where id='+id, callback);
}



exports.findId= (dataName,id,callback)=> {
	connection.query('SELECT * FROM '+dataName+' where id="'+id+'"', callback);
}


//查询某列的长度
exports.Qlength= (which,dataName,callback)=> {
	connection.query('SELECT '+which+' FROM '+dataName, callback);
}





//增加数据
exports.add=(dataName,obj,callback)=>{
	connection.query('insert into '+dataName+' set ?',obj, callback);
}


//编辑首页轮播
exports.edit_index_lunbo=(arr,callback)=>{
	connection.query('UPDATE lunbo SET index_img_src = ?,index_img_target = ?,index_status= ? WHERE id = ?',arr, callback);
}
//编辑活动轮播
exports.edit_activity_lunbo=(arr,callback)=>{
	connection.query('UPDATE lunbo SET activity_img_src = ?,activity_img_target = ?,activity_status= ? WHERE id = ?',arr, callback);
}
//编辑活动
exports.edit_activity=(arr,callback)=>{
	connection.query('UPDATE activity SET teachers = ?,school = ?,speechTitle = ?,place = ?,people = ?,sponsor = ?,content = ?,small_img_src = ?,big_img_src = ?,start_time = ?,end_time = ?,longitude = ?,latitude = ? WHERE id = ?',arr, callback);
}
//编辑专家信息
exports.edit_expert=(arr,callback)=>{
	connection.query('UPDATE experts_news2 SET html = ?,status = ? WHERE id = ?',arr, callback);
}
//编辑热点消息信息
exports.edit_news=(arr,callback)=>{
	connection.query('UPDATE experts_news2 SET html = ?,status = ? WHERE id = ?',arr, callback);
}
//编辑首页专家信息
exports.edit_index_expert=(arr,callback)=>{
	connection.query('UPDATE experts_news2 SET html = ?,status = ? WHERE id = ?',arr, callback);
}





//删除信息
exports.delete_data=(dataName,status,arr,callback)=>{
	if (typeof status !=='string') {
		status = 'status';
	}
	connection.query('UPDATE '+dataName+' SET '+status+' = ? WHERE id = ?',arr, callback);
}