/**
 * Created by Administrator on 2017/3/14.
 */
const express = require('express');
const path = require('path');

//创建路由
const Router = express.Router();

//导入相对应的控制器
const Controller = require(path.join(__dirname, '../controllers/ManagerController.js'));

//获取首页轮播信息
Router.get('/index_lunbo', Controller.get_index_lunbo);
//获取活动轮播信息
Router.get('/activity_lunbo', Controller.get_activity_lunbo);
//获取活动信息
Router.get('/activity', Controller.get_activity);
//获取专家信息
Router.get('/expert', Controller.get_expert);
//获取热点新闻信息
Router.get('/news', Controller.get_news);
//获取新增页面
Router.get('/add',Controller.getAddPage);
//获取首页专家信息
Router.get('/index_expert', Controller.get_index_expert);
//专家图片
Router.get('/expert_img', Controller.get_expert_img);




//增加首页轮播信息
Router.post('/add/index_lunbo',Controller.add_index_lunbo)
//增加活动轮播信息
Router.post('/add/activity_lunbo',Controller.add_activity_lunbo)
//增加活动信息
Router.post('/add/activity',Controller.add_activity)
//增加专家信息
Router.post('/add/expert',Controller.add_expert)
//增加热点新闻信息
Router.post('/add/news',Controller.add_news)
//增加首页专家信息
Router.post('/add/index_expert',Controller.add_index_expert)
//增加专家图片
Router.post('/add/expert_img',Controller.add_expert_img)




//获取首页轮播修改页面
Router.get('/edit/index_lunbo', Controller.getEditPage_index_lunbo);
//获取活动轮播修改页面
Router.get('/edit/activity_lunbo', Controller.getEditPage_expert_lunbo);
//获取活动修改页面
Router.get('/edit/activity', Controller.getEditPage_activity);
//获取专家修改页面
Router.get('/edit/expert', Controller.getEditPage_expert);
//获取热点消息修改页面
Router.get('/edit/news', Controller.getEditPage_news);
//获取首页专家修改页面
Router.get('/edit/index_expert', Controller.getEditPage_index_expert);



//删除页面
Router.get('/delete', Controller.delete_data);


//导出
module.exports = Router;