//处理前台的接口路由

//1.导包
const express = require('express')
const path = require('path')

//2.创建路由
const frontRouter = express.Router();

const frontController = require(path.join(__dirname,'../controllers/frontController.js'));

//首页轮播图数据
frontRouter.get('/index_lunbo',frontController.index_lunbo)

//活动轮播图数据
frontRouter.get('/activity_lunbo',frontController.activity_lunbo)

//活动数据
frontRouter.get('/activity',frontController.activity)

//详细某个活动数据
frontRouter.get('/activity/:activity_id',frontController.activity_detail)

//专家数据
frontRouter.get('/expert',frontController.expert)

//热点消息数据
frontRouter.get('/news',frontController.news)

//首页专家数据
frontRouter.get('/index_expert',frontController.index_expert)



//index.html
frontRouter.get('/index',frontController.index)

module.exports = frontRouter;