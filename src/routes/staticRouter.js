const express = require('express')
const path = require('path')

//2.创建路由
const staticRouter = express.Router();

const staticController = require(path.join(__dirname,'../controllers/staticController.js'));

//index.html
staticRouter.get('/index',staticController.index)
//app.js
staticRouter.get('/js/app.js',staticController.appJs)
//app.css
staticRouter.get('/css/app.css',staticController.appCss)
//vendor.js
staticRouter.get('/js/vendor.js',staticController.vendorJs)
//manifest.js
staticRouter.get('/js/manifest.js',staticController.manifestJs)

//处理图片请求
staticRouter.get('/:img_id',staticController.img)

module.exports = staticRouter;