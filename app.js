//1.导包
const express = require('express')
const path = require('path')

const bodyParser = require('body-parser')
const session = require('express-session')
//2.create app
const app = express();



app.use(session({ secret: 'keyboard cat', cookie: { maxAge: 600000 }})) //req.session

// parse application/x-www-form-urlencoded 
app.use(bodyParser.urlencoded({ extended: false }))
// parse application/json 
app.use(bodyParser.json())

// app.all('/zhikuang/*', function(req, res, next) {
// 	res.header("Access-Control-Allow-Origin", "/zhikuang/*");
// 	res.header("Access-Control-Allow-Headers", "X-Requested-With");
// 	res.header("Access-Control-Allow-Methods","PUT,POST,GET,DELETE,OPTIONS");
// 	res.header("X-Powered-By",' 3.2.1');
// 	res.header("Content-Type", "application/json;charset=utf-8");
// 	next();
// });

//做权限的校验
// app.all('/*',(req,res,next)=>{
// 	if (req.url == '/account/login' || req.url == '/account/register' || req.url == '/zhikuang/index_lunbo' || req.url == '/zhikuang/activity_lunbo' || req.url == '/zhikuang/activity' || req.url == '/zhikuang/expert' || req.url == '/zhikuang/news') {
// 		next();
// 	}else{
// 		if (req.session.username!=null) {
// 			next();
// 		}else{
// 			res.setHeader("Content-Type","text/html;charset=utf-8");
// 			res.end("<script>alert('您还没有登录，请先登录!');window.location.href='/account/login'</script>");
// 		}
// 	}
// })


//deal with static source
app.use(express.static(path.join(__dirname,'./src/statics')));

//deal router
const accoutRouter = require(path.join(__dirname,'./src/routes/accountRouter.js'))
const ManagerRouter=require(path.join(__dirname,'./src/routes/ManagerRouter.js'))
const frontRouter=require(path.join(__dirname,'./src/routes/frontRouter.js'))
const static=require(path.join(__dirname,'./src/routes/staticRouter.js'))
app.use('/account',accoutRouter);
app.use('/manager', ManagerRouter);

//前台接口处理
app.use('/zhikuang', frontRouter);

//图片请求处理
app.use('/images',static)

//资源请求
app.use('/static',static)

//start server
app.listen(3001 ,(err)=>{
    if (err) {
      console.log(err);
    }

    console.log("start success");
});
